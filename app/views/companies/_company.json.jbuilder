json.extract! company, :id, :name, :country, :area, :description, :created_at, :updated_at
json.url company_url(company, format: :json)
