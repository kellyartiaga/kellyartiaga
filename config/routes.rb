Rails.application.routes.draw do
  resources :companies
  devise_for :users
  get 'companies/index'
  root 'companies#index'

  resources :empresas
  resources :usuarios
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
