class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :country
      t.string :area
      t.string :description

      t.timestamps
    end
  end
end
